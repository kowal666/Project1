from django.contrib import admin
# Register your models here.
from .models import Organic


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id','NIP','Firma', 'Strona','Email','Keyword','Data','Status','Uwagi']
    search_fields = ('Firma', 'Strona','Email')


admin.site.register(Organic,CategoryAdmin)


