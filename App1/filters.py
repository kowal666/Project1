from .models import Organic
import django_filters

class UserFilter(django_filters.FilterSet):
    Firma = django_filters.CharFilter(lookup_expr='icontains')
    NIP=django_filters.NumberFilter(lookup_expr='icontains')
    Strona=django_filters.CharFilter(lookup_expr='icontains')
    class Meta:
        model = Organic
        fields = ['NIP', 'Firma', 'Strona', ]