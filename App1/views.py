from django.http import HttpResponse
from django.shortcuts import render
from .models import Organic
from .filters import UserFilter


# Create your views here.
def index(request):
    return  render(request,'app1/index.html')

def baza(request):
    Firmy=Organic.objects.order_by('Firma')
    context={'Firmy':Firmy}
    return render(request,'app1/Firmy.html',context)

def search(request):
    user_list = Organic.objects.all()
    user_filter = UserFilter(request.GET, queryset=user_list)
    return render(request, 'app1/user_list.html', {'filter': user_filter})